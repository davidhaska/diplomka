require('./bootstrap');

import React from "react";
import ReactDOM from "react-dom";

import { Router, Route, IndexRoute, browserHistory, applyRouterMiddleware } from "react-router";
import { Provider } from "react-redux";
import { useScroll } from "react-router-scroll";

import { Layout } from "./components/Layout";
import { Home } from "./components/Home";
import { OrderCreate } from "./components/OrderCreate";
import { MyOrderList } from "./components/MyOrderList";
import { OrderList } from "./components/OrderList";
import { OrderDetail } from "./components/OrderDetail";

import store from "./store";

const app = document.getElementById('app');

ReactDOM.render(
    <Provider store={ store }>
        <Router history={ browserHistory } render={ applyRouterMiddleware( useScroll() ) }>
            <Route path="/" component={ Layout }>
                <IndexRoute component={ Home }/>
                <Route path="send-order" component={ OrderCreate }/>
                <Route path="my-orders" component={ MyOrderList }/>
                <Route path="get-orders" component={ OrderList }/>
                <Route path="show-order/:id" component={ OrderDetail }/>
            </Route>
        </Router>
    </Provider>,
    app);