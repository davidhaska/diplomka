import {
    MESSAGE,
    MESSAGE_SUCCESS,
    MESSAGE_FAIL,
} from '../actions/messagesActions';

const initialState = () => {
    return {
        isSendingMessage: false,
        message: []
    }
};

export const messagesReducer = (state = initialState(), action) => {
    switch (action.type) {
        case MESSAGE: {
            return {
                ...state,
                isSendingMessage: true,
            }
        }
        case MESSAGE_SUCCESS: {
            return {
                ...state,
                isSendingMessage: false,
                message: action.payload.message,
            }
        }
        case MESSAGE_FAIL: {
            return {
                ...state,
                isSendingMessage: false,
            }
        }
        default:
            return state;
    }
};