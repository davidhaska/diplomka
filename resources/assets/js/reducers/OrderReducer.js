import {
    ORDER,
    ORDER_SUCCESS,
    ORDER_FAIL,
    ORDER_ACCEPT,
    ORDER_ACCEPT_SUCCESS,
    ORDER_ACCEPT_FAIL,
    ORDER_DECLINE,
    ORDER_DECLINE_SUCCESS,
    ORDER_DECLINE_FAIL,
    PRODUCT_ADD,
    PRODUCT_REMOVE,
} from '../actions/orderActions';

const initialState = () => {
    return {
        isOrdering: false,
        isAcceptingOrDeclining: false,
        order: []
    }
};

export const orderReducer = (state = initialState(), action) => {
    switch (action.type) {
        case ORDER: {
            return {
                ...state,
                isOrdering: true,
            }
        }
        case ORDER_SUCCESS: {
            return {
                ...state,
                isOrdering: false,
                order: [],
            }
        }
        case ORDER_FAIL: {
            return {
                ...state,
                isOrdering: false,
            }
        }
        case ORDER_ACCEPT: {
            return {
                ...state,
                isAcceptingOrDeclining: true,
            }
        }
        case ORDER_ACCEPT_SUCCESS: {
            return {
                ...state,
                isAcceptingOrDeclining: false,
            }
        }
        case ORDER_ACCEPT_FAIL: {
            return {
                ...state,
                isAcceptingOrDeclining: false,
            }
        }
        case ORDER_DECLINE: {
            return {
                ...state,
                isOrdering: false,
            }
        }
        case ORDER_DECLINE_SUCCESS: {
            return {
                ...state,
            }
        }
        case ORDER_DECLINE_FAIL: {
            return {
                ...state,
            }
        }
        case PRODUCT_ADD: {
            return {
                ...state,
                order: [...state.order, {id: action.payload.productId, quantity: action.payload.quantity}],
            }
        }
        case PRODUCT_REMOVE: {
            return {
                ...state,
                order: state.order.filter((product) => product.id !== action.payload.productId),
            }
        }
        default:
            return state;
    }
};