import axios from 'axios';

import {
    LOGIN,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
} from '../actions/authActions';

import { isNullOrUndef } from '../util';

const initialState = () => {
    let user = {};
    let email = localStorage.getItem('auth');
    let type = localStorage.getItem('auth-type');
    let isAuthenticated = false;

    if ( ! isNullOrUndef(email) && ! isNullOrUndef(type) ) {
        isAuthenticated = true;
        user = {
            email: email,
            type: type,
        };
    }

    return {
        authenticating: false,
        isAuthenticated: isAuthenticated,
        user: user
    }
};

export const authReducer = (state = initialState(), action) => {
    switch (action.type) {
        case LOGIN: {
            return {
                ...state,
                authenticating: true,
                isAuthenticated: false,
                user: null
            }
        }
        case LOGIN_SUCCESS: {
            console.log(action.payload.user);

            return {
                ...state,
                authenticating: false,
                isAuthenticated: true,
                user: action.payload.user
            }
        }
        case LOGIN_FAIL: {
            return {
                ...state,
                authenticating: false,
                isAuthenticated: false,
                user: null
            }
        }
        case LOGOUT: {
            return {
                ...state,
                authenticating: false,
                isAuthenticated: false,
                user: null
            }
        }
        default:
            return state;
    }
};