import { combineReducers } from "redux"

import { authReducer } from "./AuthReducer"
import { orderReducer } from "./OrderReducer"
import { messagesReducer } from "./MessagesReducer"

export const rootReducer = combineReducers({
    auth: authReducer,
    basket: orderReducer,
    messages: messagesReducer,
});