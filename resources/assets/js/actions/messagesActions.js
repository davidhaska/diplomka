import axios from "axios";
import {BACKEND_URL} from "../env";

export const MESSAGE = 'MESSAGE';
export const MESSAGE_SUCCESS = 'MESSAGE_SUCCESS';
export const MESSAGE_FAIL = 'MESSAGE_FAIL';

export const sendMessage = ( orderId, message, token, email ) => {
    return (dispatch) => {
        dispatch( MessageAction() );

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

        axios.post(BACKEND_URL + 'api/v1/messages', {
            order_id: orderId,
            text: message,
            email: email,
        })
            .then((response) => {
                dispatch( MessageSuccess( {message: response.data.message} ) );
            })
            .catch((error) => {
                dispatch( MessageFail( {text: error} ) );
            });
    }
};

export const MessageAction = () => ({
    type: MESSAGE,
    payload: {
        message: null,
    },
});

export const MessageSuccess = ( message ) => ({
    type: MESSAGE_SUCCESS,
    payload: {
        message: message,
    },
});

export const MessageFail = ( error ) => ({
    type: MESSAGE_FAIL,
    payload: {
        message: null,
        error: error.text,
    },
});