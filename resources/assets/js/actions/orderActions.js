import axios from 'axios';

import { BACKEND_URL } from '../env';

export const ORDER = 'ORDER';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAIL = 'ORDER_FAIL';
export const ORDER_ACCEPT = 'ORDER_ACCEPT';
export const ORDER_ACCEPT_SUCCESS = 'ORDER_ACCEPT_SUCCESS';
export const ORDER_ACCEPT_FAIL = 'ORDER_ACCEPT_FAIL';
export const ORDER_DECLINE = 'ORDER_DECLINE';
export const ORDER_DECLINE_SUCCESS = 'ORDER_DECLINE_SUCCESS';
export const ORDER_DECLINE_FAIL = 'ORDER_DECLINE_FAIL';
export const PRODUCT_ADD = 'PRODUCT_ADD';
export const PRODUCT_REMOVE = 'PRODUCT_REMOVE';

export const orderIt = ( order, token, email ) => {
    return (dispatch) => {
        dispatch( OrderAction() );

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

        axios.post(BACKEND_URL + 'api/v1/orders', {
                order: order,
                email: email,
            })
            .then((response) => {
                dispatch( OrderSuccess( {order: response.data.order.email} ) );
            })
            .catch((error) => {
                dispatch( OrderFail( {text: error} ) );
            });
    }
};

export const acceptOrder = ( orderId, token, email ) => {
    return (dispatch) => {
        dispatch( OrderAcceptAction() );

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

        axios.put(BACKEND_URL + 'api/v1/orders/' + orderId, {
            orderStatus: 'accepted',
            email: email,
        })
            .then((response) => {
                dispatch( OrderAcceptSuccess( {id: response.data.order.id} ) );
            })
            .catch((error) => {
                dispatch( OrderAcceptFail( {text: error} ) );
            });
    }
};

export const declineOrder = ( orderId, token, email ) => {
    return (dispatch) => {
        dispatch( OrderDeclineAction() );

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

        axios.put(BACKEND_URL + 'api/v1/orders/' + orderId, {
            orderStatus: 'declined',
            email: email,
        })
            .then((response) => {
                dispatch( OrderDeclineSuccess( {id: response.data.order.id} ) );
            })
            .catch((error) => {
                dispatch( OrderDeclineFail( {text: error} ) );
            });
    }
};

export const addProductToOrder = ( productId, quantity ) => {
    return (dispatch) => {
        dispatch( AddProduct(productId, quantity) );
    }
};

export const removeProductFromOrder = ( productId ) => {
    return (dispatch) => {
        dispatch( RemoveProduct(productId) );
    }
};

export const OrderAction = () => ({
    type: ORDER,
    payload: {
        order: null,
    },
});

export const OrderSuccess = ( order ) => ({
    type: ORDER_SUCCESS,
    payload: {
        order: order,
    },
});

export const OrderAcceptAction = () => ({
    type: ORDER_ACCEPT,
    payload: {
        order: null,
    },
});

export const OrderAcceptSuccess = ( order ) => ({
    type: ORDER_ACCEPT_SUCCESS,
    payload: {
        order: order,
    },
});

export const OrderAcceptFail = ( error ) => ({
    type: ORDER_ACCEPT_FAIL,
    payload: {
        order: null,
        error: error.text,
    },
});

export const OrderDeclineAction = () => ({
    type: ORDER_DECLINE,
    payload: {
        order: null,
    },
});

export const OrderDeclineSuccess = ( order ) => ({
    type: ORDER_DECLINE_SUCCESS,
    payload: {
        order: order,
    },
});

export const OrderDeclineFail = ( error ) => ({
    type: ORDER_DECLINE_FAIL,
    payload: {
        order: null,
        error: error.text,
    },
});

export const OrderFail = ( error ) => ({
    type: ORDER_FAIL,
    payload: {
        order: null,
        error: error.text,
    },
});

export const AddProduct = ( productId, quantity ) => ({
    type: PRODUCT_ADD,
    payload: {
        productId: productId,
        quantity: quantity,
    },
});

export const RemoveProduct = ( productId ) => ({
    type: PRODUCT_REMOVE,
    payload: {
        productId: productId,
    },
});