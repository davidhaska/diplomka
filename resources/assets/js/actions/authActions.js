import axios from 'axios';

import { BACKEND_URL } from '../env';

export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT = 'LOGOUT';

export const logIn = ( user ) => {
    return (dispatch) => {
        dispatch( LogInAction() );

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = user.token;

        axios.post(BACKEND_URL + 'api/v1/login', {
                email: user.email,
                password: user.password,
            })
            .then((response) => {
                localStorage.setItem('auth', response.data.user.email);
                localStorage.setItem('auth-type', response.data.user.type);
                dispatch( LogInSuccess( {email: response.data.user.email, type: response.data.user.type} ) );
            })
            .catch((error) => {
                localStorage.setItem('auth', null);
                localStorage.setItem('auth-type', null);
                dispatch( LogInFail( {text: error} ) );
            });
    }
};

export const logOut = () => {
    return (dispatch) => {
        localStorage.setItem('auth', null);
        localStorage.setItem('auth-type', null);
        dispatch( Logout() );
    }
};

export const LogInAction = () => ({
    type: LOGIN,
    payload: {
        user: null,
    },
});

export const LogInSuccess = ( user ) => ({
    type: LOGIN_SUCCESS,
    payload: {
        user: user,
    },
});

export const LogInFail = ( error ) => ({
    type: LOGIN_FAIL,
    payload: {
        user: null,
        error: error.text,
    },
});

export const Logout = () => ({
    type: LOGOUT,
    payload:  {
        user: null,
    },
});