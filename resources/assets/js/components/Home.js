import React from 'react';

import { connect } from 'react-redux';

export class HomeRaw extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return(
            <div>
                <h1>Autor: Bc. David Haška</h1>
            </div>
        );
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export const Home = connect(
    mapStateToProps,
    mapDispatchToProps,
)(HomeRaw);