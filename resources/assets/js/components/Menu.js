import React from "react";

import { Link } from "react-router";
import { connect } from 'react-redux';
import { logOut } from '../actions/authActions';

export class MenuRaw extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.handleClickLogout = this.handleClickLogout.bind(this);
    }

    handleClickLogout(event) {
        event.preventDefault();

        this.props.logOut();
    }

    render() {

        const { user } = this.props;

        return(
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/">Diplomová práce</a>
                    </div>
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        {user.type == 'customer' ? (
                            <ul className="nav navbar-nav">
                                <li className={window.location.pathname == 'my-orders' ? 'active' : ''}><Link to="my-orders">Moje objednávky</Link></li>
                                <li className={window.location.pathname == 'send-order' ? 'active' : ''}><Link to="send-order">Odeslat objednávku</Link></li>
                            </ul>
                        ) : (
                            <ul className="nav navbar-nav">
                                <li className={window.location.pathname == 'get-orders' ? 'active' : ''}><Link to="get-orders">Zobrazit objednávky</Link></li>
                            </ul>
                        )}

                        <ul className="nav navbar-nav navbar-right">
                            <li className="dropdown">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">{user.email} <span
                                    className="caret"></span></a>
                                <ul className="dropdown-menu">
                                    <li><Link onClick={this.handleClickLogout}>Odhlásit se</Link></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
});

const mapDispatchToProps = {
    logOut,
};

export const Menu = connect(
    mapStateToProps,
    mapDispatchToProps,
)(MenuRaw);