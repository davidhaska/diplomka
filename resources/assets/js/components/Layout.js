import axios from 'axios';

import React from 'react';

import { connect } from 'react-redux';

import { logOut } from '../actions/authActions';

import { LoginForm } from './LoginForm';
import { Menu } from './Menu';
import { BACKEND_URL } from '../env';

export class LayoutRaw extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            token: '',
        };

        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        this.setState({ token: token });

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

        axios.get(BACKEND_URL + 'api/v1/user')
            .then((response) => {
                if (response.data.user === null) {
                    this.props.logOut();
                }
            })
            .catch((error) => {
                this.setState({ errors: error });
            });
    }

    handleChangeEmail(event) {
        this.setState({ email: event.target.value });
    }

    handleChangePassword(event) {
        this.setState({ password: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();

        let submit = {
            email: this.state.email,
            password: this.state.password,
            token: this.state.token,
        };

        this.setState({ email: '' });
        this.setState({ password: '' });

        this.props.logIn(submit);

    }

    render() {
        const { authenticating, isAuthenticated } = this.props;

        const containerStyle = {
            marginTop: "60px"
        };

        let menu = null;
        let content = null;
        let layout = "col-md-12";

        if (authenticating) {
            content = "Přihlašuji...";
        } else {
            if  ( ! isAuthenticated) {
                layout = "col-md-4 col-md-offset-4";
                content = <LoginForm/>;
            } else {
                menu = <Menu/>;
                content = this.props.children;
            }
        }

        return(
            <div>
                {menu}
                <div className="container" style={containerStyle}>
                    <div className="row">
                        <div className={layout}>
                            {content}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    authenticating: state.auth.authenticating,
    isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = {
    logOut,
};

export const Layout = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LayoutRaw);