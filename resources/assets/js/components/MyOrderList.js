import axios from 'axios';

import React from 'react';

import { Link, browserHistory } from "react-router";
import { connect } from 'react-redux';

import { acceptOrder, declineOrder } from '../actions/orderActions';

import { isNullOrUndef } from '../util';
import { BACKEND_URL } from "../env";

export class MyOrderListRaw extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            orders: [],
            errors: null,
            isLoaded: false,
        };
    }

    componentDidMount() {
        const { user } = this.props;

        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

        axios.get(BACKEND_URL + 'api/v1/orders', {
                params: {
                    email: user.email,
                }
            })
            .then((response) => {
                this.setState({ orders: response.data.orders });
                this.setState({ errors: null });
                this.setState({ isLoaded: true });
            })
            .catch((error) => {
                this.setState({ orders: [] });
                this.setState({ errors: error });
                this.setState({ isLoaded: false });
            });

        Echo.channel('orders')
            .listen('OrderWasCreated', (data) => {
                this.AddCreatedOrderToList(data.order.content);
            })
            .listen('OrderWasUpdated', (data) => {
                this.ChangeOrderStatus(data.order.id, data.order.status);
            });
    }

    AddCreatedOrderToList(order) {
        this.setState({ orders: [...this.state.orders, order] });
    }

    ChangeOrderStatus(orderId, status) {
        let oldOrders = this.state.orders.filter(order => order.id != orderId);
        let newOrder = this.state.orders.find(order => order.id == orderId);

        newOrder.status = status;

        this.setState({ orders: [...oldOrders, newOrder] });
    }

    CheckUserTypeForButtons(type, orderId, status) {
        let content = '';

        if (type == 'supplier') {
            if (status == 'new') {
                content = [
                    <input type="button"
                         className="btn btn-success"
                         id="order"
                         value="Potvrdit objednávku"
                         onClick={(e) => this.handleClickAcceptOrder(orderId, e)}
                         key={'accept' + orderId}/>,
                    <input type="button"
                        className="btn btn-danger"
                        id="order"
                        value="Zrušit objednávku"
                        onClick={(e) => this.handleClickDeclineOrder(orderId, e)}
                        key={'decline' + orderId}/>
                ];
            } else if (status == 'accepted') {
                content = <span className="alert alert-success" style={{padding:"0"}}>Objednávka potvrzena</span>;
            } else if (status == 'declined') {
                content = <span className="alert alert-danger" style={{padding:"0"}}>Objednávka zrušena</span>;
            }
        } else {
            if (status == 'new') {
                content = <span className="alert alert-info" style={{padding:"0"}}>Nová objednávka</span>;
            } else if (status == 'accepted') {
                content = <span className="alert alert-success" style={{padding:"0"}}>Objednávka potvrzena</span>;
            } else if (status == 'declined') {
                content = <span className="alert alert-danger" style={{padding:"0"}}>Objednávka zrušena</span>;
            }
        }

        return content;
    }

    handleClickAcceptOrder(orderId, event) {
        event.preventDefault();

        const { user } = this.props;

        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        this.props.acceptOrder(orderId, token, user.email);

        this.ChangeOrderStatus(orderId, 'accepted');
    }

    handleClickDeclineOrder(orderId, event) {
        event.preventDefault();

        const { user } = this.props;

        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        this.props.declineOrder(orderId, token, user.email);

        this.ChangeOrderStatus(orderId, 'declined');
    }

    render() {
        const { user } = this.props;

        let loading = '';
        let orders = [];
        let errors = '';

        if ( isNullOrUndef(this.state.orders) || this.state.orders.length < 1 ) {
            if (this.state.isLoaded) {
                loading = <div className="alert alert-warning">Žádné objednávky nejsou k dispozici.</div>;
            } else {
                loading = <div className="alert alert-info">Načítám objednávky...</div>;
                orders = [];
            }
        } else {
            orders = this.state.orders;
        }

        if ( ! isNullOrUndef(this.state.errors) ) {
            errors = <div className="alert alert-danger">{this.state.errors}</div>;
        }


        return(
            <div>
                <h1>Objednávky - <Link onClick={browserHistory.goBack}>zpět</Link></h1>
                {errors}
                <form>
                    {loading}
                    {orders
                        .sort((a,b) => b.id - a.id)
                        .map(order => (
                        <div key={order.id}>
                            <h2><Link to={`/show-order/${ order.id }`}>Detail</Link> objednávky číslo: {order.id}</h2>
                            <h3>Celková cena: {order.total_price} Kč</h3>
                            <div className="form-group">
                                {this.CheckUserTypeForButtons(user.type, order.id, order.status)}
                                {(typeof (order.products) !== undefined) ? (
                                        <ul>
                                        {order.products
                                            .sort((a,b) => a.id - b.id)
                                            .map(product => (
                                            <li key={product.id}>
                                                {product.name} - {product.price} Kč
                                            </li>
                                        ))}
                                        </ul>
                                    ) : (
                                        <span>Načítám produkty...</span>
                                )}
                            </div>
                        </div>
                    ))}
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
});

const mapDispatchToProps = {
    acceptOrder,
    declineOrder,
};

export const MyOrderList = connect(
    mapStateToProps,
    mapDispatchToProps,
)(MyOrderListRaw);