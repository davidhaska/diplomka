import './MessageList.css';

import React from 'react';

import { connect } from 'react-redux';

import { sendMessage } from '../actions/messagesActions';

import { isNullOrUndef } from '../util';

export class MessageListRaw extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            message: '',
            errors: null,
        };
    }

    componentDidMount() {
        Echo.channel('messages.' + this.props.orderId)
            .listen('MessageWasCreated', (data) => {
                this.AddCreatedMessageToList(data.message.content);
            });
    }

    AddCreatedMessageToList(message) {
        this.props.addMessage(message);
    }

    CheckStatus(status) {
        let content = '';

        if (status == 'new') {
            content = <span className="alert alert-info" style={{padding:"0"}}>Nová zpráva</span>;
        }

        return content;
    }

    handleClickSendMessage(orderId, event) {
        event.preventDefault();

        const { user } = this.props;

        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        let text = this.state.message;
        this.setState({ message: '' });

        this.props.sendMessage(orderId, text, token, user.email);
    }

    render() {
        const { user, messages, orderId } = this.props;

        let loading = '';
        let errors = '';

        if ( isNullOrUndef(messages) || messages.length < 1 ) {
            loading = <div className="alert alert-info">Neexistují žádné zprávy k této objednávce</div>;
        }

        if ( ! isNullOrUndef(this.state.errors) ) {
            errors = <div className="alert alert-danger">{this.state.errors}</div>;
        }

        return(
            <div>
                <h1>Zprávy</h1>
                {errors}
                {loading}
                {messages
                    .sort((a,b) => new Date(a.created_at).getTime() -
                    new Date(b.created_at).getTime())
                    .map(message => (
                        <div key={message.id} className={(user.email == message.sender.email ? 'myMessage' : 'notMyMessage')}>
                            {(user.email == message.sender.email ? '' : <span className="messageAuthor">Od: {message.sender.email}</span>)}
                            <span>{message.text}</span>
                        </div>
                    ))}
                <form>
                    <div className="form-group form-new-message">
                        <label htmlFor="messageInput">Nová zpráva</label>
                        <textArea className="form-control"
                           name="message"
                           id="messageInput"
                           value={this.state.message}
                           onChange={(e) => this.setState({ [e.target.name]: e.target.value })}/>
                    </div>
                    <div className="form-check">
                        <button type="submit"
                            className="btn btn-primary"
                            onClick={(e) => this.handleClickSendMessage(orderId, e)}>Odeslat zprávu</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
});

const mapDispatchToProps = {
    sendMessage,
};

export const MessageList = connect(
    mapStateToProps,
    mapDispatchToProps,
)(MessageListRaw);