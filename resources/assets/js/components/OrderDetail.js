import axios from 'axios';

import React from 'react';

import { Link, browserHistory } from "react-router";
import { connect } from 'react-redux';

import { acceptOrder, declineOrder } from '../actions/orderActions';

import { MessageList } from "./MessageList";

import { isNullOrUndef } from '../util';
import { BACKEND_URL } from "../env";

export class OrderDetailRaw extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            order: null,
            errors: null
        };
    }

    componentDidMount() {
        const { user } = this.props;

        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

        axios.get(BACKEND_URL + 'api/v1/orders/' + this.props.params.id, {
            params: {
                email: user.email,
            }
        })
            .then((response) => {
                this.setState({ order: response.data.order });
                this.setState({ errors: null });

            })
            .catch((error) => {
                this.setState({ order: null });
                this.setState({ errors: error });
            });
    }

    handleClickSendMessage(orderId, event) {
        event.preventDefault();

        /*const { user } = this.props;

        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        this.props.acceptOrder(orderId, token, user.email);

        let newOrders = this.state.orders.filter(order => order.id != orderId);
        let newOrder = this.state.orders.find(order => order.id == orderId);

        newOrder.status = 'accepted';

        this.setState({ orders: [...newOrders, newOrder] });*/
    }

    AddCreatedMessageToMessages(message) {
        let order = {...this.state.order};
        order.messages = [...this.state.order.messages, message];

        this.setState({ order });
    }

    render() {
        let content = '';
        let products = [];
        let messages = [];
        let messagesLoading = '';
        let errors = '';

        if ( isNullOrUndef(this.state.order) ) {
            content = <div className="alert alert-info">Načítám objednávku...</div>;
            products = [];
        } else {
            content = <div key={this.state.order.id}>
                <h2>Ojednávka číslo: {this.state.order.id}</h2>
                <h3>Celková cena: {this.state.order.total_price} Kč</h3>
            </div>;
            products = this.state.order.products;
        }

        if ( isNullOrUndef(this.state.order) ) {
            messages = [];
            messagesLoading = <div className="alert alert-info">Načítám zprávy...</div>;
        } else {
            messages = this.state.order.messages;
            messagesLoading = <MessageList orderId={this.state.order.id} messages={messages} addMessage={this.AddCreatedMessageToMessages.bind(this)}/>;
        }

        if ( ! isNullOrUndef(this.state.errors) ) {
            errors = <div className="alert alert-danger">{this.state.errors}</div>;
        }

        return(
            <div>
                <h1>Objednávka - <Link onClick={browserHistory.goBack}>zpět</Link></h1>
                {errors}
                <div>
                    {content}
                    {products
                        .sort((a,b) => a.id - b.id)
                        .map(product => (
                            <div key={product.id}>
                                {product.name} -  Cena: {product.pivot.price} Kč - Počet: {product.pivot.quantity}
                            </div>
                        ))};
                    {messagesLoading}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
});

const mapDispatchToProps = {
    //sendMessage,
};

export const OrderDetail = connect(
    mapStateToProps,
    mapDispatchToProps,
)(OrderDetailRaw);