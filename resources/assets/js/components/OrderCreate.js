import axios from 'axios';

import React from 'react';

import { browserHistory, Link } from "react-router";
import { connect } from 'react-redux';

import { orderIt, addProductToOrder, removeProductFromOrder } from '../actions/orderActions';

import { isNullOrUndef } from '../util';
import { BACKEND_URL } from "../env";

export class OrderCreateRaw extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            productId: null,
            errors: null,
            isChecked: false,
        };
    }

    componentDidMount() {
        const { user } = this.props;

        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

        axios.get(BACKEND_URL + 'api/v1/products', {
                params: {
                    email: user.email,
                }
            })
            .then((response) => {
                response.data.products.map(product => (
                    this.setState({ [product.id]: parseInt(product.default_quantity) })
                ));

                this.setState({ products: response.data.products });
                this.setState({ errors: null });
            })
            .catch((error) => {
                this.setState({ products: [] });
                this.setState({ errors: error });
            });
    }

    getQuantity(productId) {
        let product = this.props.order.find((product) => {
            return product.id === productId;
        });

        return product.quantity;
    }

    getPrice(productId) {
        let product = this.state.products.find((product) => {
            return product.id === productId;
        });

        return parseFloat(product.price);
    }

    handleProductQuantity(productId, event) {
        event.preventDefault();

        this.setState({ [productId]: parseInt(event.target.value) });
    }

    checkOrderProduct(productId) {
        return this.props.order.some(product => productId === product.id);
    }

    getOrderTotal() {
        let total = 0.0;

        this.props.order.map(product => (
            total = total + parseFloat(this.getPrice(product.id))
        ));

        return total;
    }

    handleClickAddProductToOrder(productId, event) {
        event.preventDefault();

        this.props.addProductToOrder(productId, this.state[productId]);
    }

    handleClickRemoveProductFromOrder(productId, event) {
        event.preventDefault();

        this.props.removeProductFromOrder(productId);
    }

    handleSubmit(event) {
        event.preventDefault();

        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        this.props.orderIt(this.props.order, token);
    }

    render() {
        let loading = '';
        let errors = '';
        let total = '';

        if ( this.state.products.length < 1 ) {
            loading = <div className="alert alert-info">Načítám produkty...</div>;
        }

        if ( ! isNullOrUndef(this.state.errors) ) {
            errors = <div className="alert alert-danger">{this.state.errors}</div>;
        }

        if ( this.props.order.length > 0 ) {
            total = <h2>Celková cena objednávky: {this.getOrderTotal()} Kč</h2>;
        }

        return(
            <div>
                <h1>Vytvořit objednávku - <Link onClick={browserHistory.goBack}>zpět</Link></h1>
                {errors}
                {total}
                { ! this.props.isOrdering &&
                    <form>
                        {loading}
                        {this.state.products
                            .sort((a,b) => a.id - b.id)
                            .map(product => (
                            <div className="form-group" key={product.id}>
                                <label htmlFor="product">{product.name}</label>
                                {this.checkOrderProduct(product.id) ? (
                                    <span> - množství: {this.getQuantity(product.id)} </span>
                                ) : (
                                    <input type="number"
                                           min={1}
                                           className="form-control"
                                           id="product"
                                           value={this.state[product.id]}
                                           onChange={(e) => this.handleProductQuantity(product.id, e)}/>
                                )}
                                {this.checkOrderProduct(product.id) ? (
                                    <input type="button"
                                           className="btn btn-danger"
                                           id="product"
                                           value="Odebrat z košíku"
                                           onClick={(e) => this.handleClickRemoveProductFromOrder(product.id, e)}/>
                                ) : (
                                    <input type="button"
                                           className="btn btn-success"
                                           id="product"
                                           value="Přidat do košíku"
                                           onClick={(e) => this.handleClickAddProductToOrder(product.id, e)}/>
                                )}
                            </div>
                        ))}
                        {this.props.order.length > 0 &&
                        <div className="form-check">
                            <button type="submit"
                                    className="btn btn-primary"
                                    onClick={(e) => this.handleSubmit(e)}>Odeslat objednávku
                            </button>
                        </div>
                        }
                    </form>
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
    order: state.basket.order,
    isOrdering: state.basket.isOrdering,
});

const mapDispatchToProps = {
    orderIt,
    addProductToOrder,
    removeProductFromOrder,
};

export const OrderCreate = connect(
    mapStateToProps,
    mapDispatchToProps,
)(OrderCreateRaw);