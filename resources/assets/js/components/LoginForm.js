import React from "react";

import { connect } from 'react-redux';

import { logIn } from '../actions/authActions';

export class LoginFormRaw extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            token: '',
        };
    }

    componentDidMount() {
        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        this.setState({ token: token });
    }

    handleSubmit(event) {
        event.preventDefault();

        let submit = {
            email: this.state.email,
            password: this.state.password,
            token: this.state.token,
        };

        this.setState({ email: '' });
        this.setState({ password: '' });

        this.props.logIn(submit);
    }

    render() {
        return(
            <form>
                <div className="form-group">
                    <label htmlFor="emailInput">Emailová adresa</label>
                    <input type="email"
                           className="form-control"
                           name="email"
                           id="emailInput"
                           placeholder="Napište Vaší emailovou adresu"
                           value={this.state.email}
                           onChange={(e) => this.setState({ [e.target.name]: e.target.value })}/>
                </div>
                <div className="form-group">
                    <label htmlFor="passwordInput">Heslo</label>
                    <input type="password"
                           className="form-control"
                           name="password"
                           id="passwordInput"
                           placeholder="Napište Vaše heslo"
                           value={this.state.password}
                           onChange={(e) => this.setState({ [e.target.name]: e.target.value })}/>
                </div>
                <div className="form-check">
                    <button type="submit"
                            className="btn btn-primary"
                            onClick={(e) => this.handleSubmit(e)}>Přihlásit se</button>
                </div>
            </form>
        );
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
    logIn,
};

export const LoginForm = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LoginFormRaw);