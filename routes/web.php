<?php

Route::prefix('api')->name('api.')->group(function () {
    Route::prefix('v1')->name('v1.')->group(function () {
        Route::post('login', 'Auth\LoginController@login')->name('login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');
        Route::get('user', 'Auth\LoginController@user')->name('user');

        Route::resource('orders', 'OrderController')->only([
            'index', 'store', 'show', 'update', 'destroy'
        ]);

        Route::resource('products', 'ProductController')->only([
            'index', 'store', 'show', 'update', 'destroy'
        ]);

        Route::resource('messages', 'MessageController')->only([
            'index', 'store', 'show', 'update', 'destroy'
        ]);

        Route::resource('users', 'UserController')->only([
            'index', 'store', 'show', 'update', 'destroy'
        ]);

        Route::get('test', function() {
            event(new \App\Events\OrderWasCreated(['id'=>1,'message'=>'text']));

           return 'test fired';
        });
    });
});

Route::get('/{path?}', function($path = null) {
    return view('welcome');
})->where('path', '.*');
