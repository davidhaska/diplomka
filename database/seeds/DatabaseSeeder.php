<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ( ! \App\User::where('email', 'zakaznik@diplomka.cz')->exists() )
        {
            $user = new \App\User;
            $user->email = 'zakaznik@diplomka.cz';
            $user->password = bcrypt('test123');
            $user->type = 'customer';
            $user->save();
        }

        if ( ! \App\User::where('email', 'odberatel@diplomka.cz')->exists() )
        {
            $user = new \App\User;
            $user->email = 'odberatel@diplomka.cz';
            $user->password = bcrypt('test123');
            $user->type = 'customer';
            $user->save();
        }

        if ( ! \App\User::where('email', 'dodavatel@diplomka.cz')->exists() )
        {
            $user = new \App\User;
            $user->email = 'dodavatel@diplomka.cz';
            $user->password = bcrypt('test123');
            $user->type = 'supplier';
            $user->save();
        }

        if ( ! \App\Product::where('name', 'Produkt 1')->exists() )
        {
            $product = new \App\Product;
            $product->name = 'Produkt 1';
            $product->price = 200.00;
            $product->default_quantity = 1;
            $product->save();
        }

        if ( ! \App\Product::where('name', 'Produkt 2')->exists() )
        {
            $product = new \App\Product;
            $product->name = 'Produkt 2';
            $product->price = 400.00;
            $product->default_quantity = 1;
            $product->save();
        }
    }
}
