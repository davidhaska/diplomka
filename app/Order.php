<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'total_price', 'status',
    ];

    /**
     * The products that belong to the order.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product', 'order_id', 'product_id')
            ->withPivot(['price', 'quantity'])
            ->withTimestamps();
    }

    /**
     * Get the user that sent the order.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the message for the order.
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
