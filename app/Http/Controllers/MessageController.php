<?php

namespace App\Http\Controllers;

use App\Events\MessageWasCreated;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();

        $this->checkEmail($request->email, $user->email);

        $order_id = $request->order_id;
        $sender_id = $user->id;
        $text = $request->text;

        $savedMessage = Message::create([
            'order_id' => $order_id,
            'sender_id' => $sender_id,
            'text' => $text,
            'status' => 'new',
        ]);

        if ($savedMessage)
        {
            $savedMessage->sender()->associate($user);

            $completeMessage = $savedMessage->load('sender');

            event( new MessageWasCreated( ['content' => $completeMessage] ) );

            return response()->json([
                'success' => true,
                'message' => $completeMessage,
            ], 200);
        }

        return response()->json([
            'success' => false,
        ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkEmail($requestEmail, $sessionEmail)
    {
        if ($requestEmail != $sessionEmail)
        {
            return response()->json([
                'success' => false,
                'messages' => [],
            ], 401);
        }
    }
}
