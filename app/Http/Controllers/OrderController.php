<?php

namespace App\Http\Controllers;

use App\Events\OrderWasCreated;
use App\Events\OrderWasUpdated;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $this->checkEmail($request->email, $user->email);

        if ($user->type == 'customer') {
            return response()->json([
                'success' => true,
                'orders' => $user->orders()->with('products')->get(),
            ], 200);
        }

        return response()->json([
            'success' => true,
            'orders' => Order::with('products')->get(),
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();

        $this->checkEmail($request->email, $user->email);

        $total_price = 0.00;

        $productIds = [];

        $products = $request->order;

        foreach ($products as $product)
        {
            $dbProduct = Product::find($product['id']);

            if ($dbProduct)
            {
                $productIds[$dbProduct->id] = [
                    'price' => $dbProduct->price,
                    'quantity' => $product['quantity'],
                ];

                $total_price += $dbProduct->price;
            }
        }

        $savedOrder = Order::create([
            'user_id' => $user->id,
            'total_price' => $total_price,
            'status' => 'new',
        ]);

        if ($savedOrder)
        {
            $savedOrder->products()->attach($productIds);

            $completeOrder = $savedOrder->load('products');

            event( new OrderWasCreated( ['content' => $completeOrder] ) );

            return response()->json([
                'success' => true,
                'order' => $completeOrder,
            ], 200);
        }

        return response()->json([
            'success' => false,
        ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = $request->user();

        $this->checkEmail($request->email, $user->email);

        return response()->json([
            'success' => true,
            'order' => Order::where('id', $id)->with(['user','products','messages.sender'])->first(),
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $request->user();

        $this->checkEmail($request->email, $user->email);

        $status = $request->orderStatus;

        $order = Order::find($id);
        $order->status = $status;
        $saved = $order->save();

        if ($saved)
        {
            event( new OrderWasUpdated($order) );

            return response()->json([
                'success' => true,
                'order' => $order,
            ], 200);
        }

        return response()->json([
            'success' => false,
        ], 500);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkEmail($requestEmail, $sessionEmail)
    {
        if ($requestEmail != $sessionEmail)
        {
            return response()->json([
                'success' => false,
                'orders' => [],
            ], 401);
        }
    }
}
