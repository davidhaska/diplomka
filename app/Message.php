<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'sender_id',
        'text',
        'status',
    ];

    /**
     * Get the sender that sent the message.
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    /**
     * Get the order that belongs to the message.
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
